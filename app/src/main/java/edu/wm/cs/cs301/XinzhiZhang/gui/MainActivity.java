package edu.wm.cs.cs301.XinzhiZhang.gui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import edu.wm.cs.cs301.XinzhiZhang.R;

public class MainActivity extends AppCompatActivity {

    /**
     * Called once start the app. Switch from the main activity to state title/AMazeActivity immediately.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent = new Intent(this, AMazeActivity.class);
        startActivity(intent);
    }
}
