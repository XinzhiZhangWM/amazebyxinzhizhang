package edu.wm.cs.cs301.XinzhiZhang.gui;

import java.util.Arrays;
import edu.wm.cs.cs301.XinzhiZhang.generation.CardinalDirection;
import edu.wm.cs.cs301.XinzhiZhang.generation.Floorplan;
import edu.wm.cs.cs301.XinzhiZhang.generation.Maze;
import edu.wm.cs.cs301.XinzhiZhang.gui.Constants.UserInput;
/**
 * This class implements the Robot class, it provides basic functions of a robot,
 * including sensor and acutator. It also provides the idea of batterylevel, and odometer.
 * CRC card:
 * Responsibility: implement Robot.java to provide basic functions of a robot.
 * Collaborators: Controller.java, StateWinning.java, StatePlaying.java, Floorplan.java
 * FutureUsage: Robot driver algorithms like Wall-Follower and Wizard will operate on top of this class.
 * @author Lulu Zhang
 *
 */
public class BasicRobot implements Robot {

	private float batteryLevel;
	private int odoMeter;
	private boolean leftSensor;
	private boolean rightSensor;
	private boolean forwardSensor;
	private boolean backwardSensor;
	private boolean roomSensor;
	private boolean exitSensor;
	private boolean hitWall;
	private boolean [] directionSensors = {leftSensor, rightSensor, forwardSensor, backwardSensor};
	private Maze maze;
	private StatePlaying statePlaying;
	private PlayAnimationActivity playAnimationActivity;

	/**
	 *Constructor that has no parameters that creates a robot with all possible sensors operational and a fully loaded battery.
	 */
	public BasicRobot() {
		init();
	}

	/**
	 * Constructor creates a robot. It sets controller, maze, sensors, odometer, and hitwall
	 * @param
	 */
	public BasicRobot(StatePlaying statePlaying) {
		this.statePlaying = statePlaying;
		this.setMaze(statePlaying.getMazeConfiguration());
		init();
	}

	/**
	 * Helper function to save spaces in the constructors.
	 */
	private void init() {
		this.batteryLevel = 3000;
		this.leftSensor = true;
		this.rightSensor = true;
		this.forwardSensor = true;
		this.backwardSensor = true;
		directionSensors[0] = leftSensor;
		directionSensors[1] = rightSensor;
		directionSensors[2] = forwardSensor;
		directionSensors[3] = backwardSensor;
		this.roomSensor = true;
		this.exitSensor = true;
		this.hitWall = false;
		this.odoMeter = 0;
	}

	/**
	 * Get current position of the robot. Throw exceptions when the position is invalid.
	 * @return An array which represents its current position
	 */
	@Override
	public int[] getCurrentPosition() throws Exception {
		int[] currentPos = this.statePlaying.getCurrentPosition();
		if (currentPos[0] < 0 || currentPos[0] > this.maze.getWidth()) {
			throw new Exception("Position is outside of the maze");
		}
		if (currentPos[1] < 0 || currentPos[1] > this.maze.getHeight()) {
			throw new Exception("Position is outside of the maze");
		}
		return currentPos;
	}

	/**
	 * Get current cardinal direction of the robot, not robot's relative direction.
	 * @return Robot's cardinal direction
	 */
	@Override
	public CardinalDirection getCurrentDirection() {
		CardinalDirection currentDir = this.statePlaying.getCurrentDirection();
		return currentDir;
	}

	/**
	 * Give the underlying maze to the robot.
	 */
	@Override
	public void setMaze(Maze maze) {
		//if (controller != null && controller.currentState == controller.states[2]) {
		this.maze = maze;
		//}
	}

	public Maze getMaze() {
		return this.maze;
	}

	public void setPlayAnimationActivity(PlayAnimationActivity playAnimationActivity){
		this.playAnimationActivity = playAnimationActivity;
	}

	public void setStatePlaying(StatePlaying statePlaying){
		this.statePlaying = statePlaying;
	}


	/**
	 * Get robot's current battery level.
	 * @return The robot's current battery level.
	 */
	@Override
	public float getBatteryLevel() {
		return this.batteryLevel;
	}
	/**
	 * Set the robot's current battery level to the passed parameter
	 */
	@Override
	public void setBatteryLevel(float level) {
		if (level >= 0) {
			this.batteryLevel = level;
		}
	}

	/**
	 * Get robot's odometer, i.e., the distance robot has moved
	 * @return The robot's current path length.
	 */
	@Override
	public int getOdometerReading() {
		return this.odoMeter;
	}

	/**
	 * Reset the robot's odometer to 0.
	 */
	@Override
	public void resetOdometer() {
		this.odoMeter = 0;
		//Use assert() statement to check whether odometer is reset successfully.
		assert this.odoMeter == 0;

	}

	/**
	 * Give energy consumption when rotate 360 degrees.
	 * @return 3*4 = 12
	 */
	@Override
	public float getEnergyForFullRotation() {
		return 12;
	}

	/**
	 * Give energy consumption when move 1 step.
	 * @return 1*5 = 5
	 */
	@Override
	public float getEnergyForStepForward() {
		return 5;
	}

	/**
	 * Use the floorplan to find whether the robot is at exit or not
	 */
	@Override
	public boolean isAtExit(){
		int x = this.statePlaying.getCurrentPosition()[0];
		int y = this.statePlaying.getCurrentPosition()[1];
		Floorplan floorplan = this.maze.getFloorplan();
		if (this.maze.isValidPosition(x, y)) {
			return floorplan.isExitPosition(x, y);
		}
		return false;
	}

	/**
	 * Use the distance determined by distanceToObstacle method to find whether the robot is facing the exit
	 */
	@Override
	public boolean canSeeThroughTheExitIntoEternity(Direction direction) {
		int x = this.statePlaying.getCurrentPosition()[0];
		int y = this.statePlaying.getCurrentPosition()[1];
		CardinalDirection currentDir = this.getCurrentDirection();
		CardinalDirection newDir = null;
		if (currentDir == CardinalDirection.East || currentDir == CardinalDirection.West) {
			newDir = this.eastAndWestFindNewDirection(direction);
		}
		else if (currentDir == CardinalDirection.North || currentDir == CardinalDirection.South) {
			newDir = this.northAndSouthfindNewDirection(direction);
		}

		if (direction != Direction.FORWARD) {
			assert currentDir != newDir;   //assert() statement to check the cardinal direction with the given relative direction
		}
		int curx = x, cury = y;

		while (this.maze.isValidPosition(curx, cury)) {
			if (this.maze.hasWall(curx, cury, newDir) == true) {
				//System.out.println(distance);
				this.batteryLevel --;
				//this.switchFromPlayingToLosingIfNecessary(x, y);
				return false;
			}
			switch(newDir) {
			case East:
				curx ++;
				break;
			case West:
				//System.out.println("case west basic robot");
				curx --;
				break;
			case North:
				cury --;
				break;
			case South:
				cury++;
				break;
			}
			assert curx != x || cury != y; //Use assert() statement to check whether the current position is moving to detect obstacles.
		}
		this.batteryLevel --;
		//this.switchFromPlayingToLosingIfNecessary(x, y);
		return true;
	}
	/**
	 * Use the floorplan to find whether the robot is at exit or not
	 */
	@Override
	public boolean isInsideRoom() throws UnsupportedOperationException {
		if (roomSensor == false) {
			throw new UnsupportedOperationException("Room sensor is not supported by robot");
		}
		int x = this.statePlaying.getCurrentPosition()[0];
		int y = this.statePlaying.getCurrentPosition()[1];
		//Use assert statement to make sure the position is valid.
		assert x < this.maze.getWidth();
		assert x >= 0;
		assert y < this.maze.getHeight();
		assert y >= 0;
		Floorplan floorplan = this.maze.getFloorplan();
		return floorplan.isInRoom(x, y);
	}

	@Override
	public boolean hasRoomSensor() {
		return this.roomSensor;
	}

	@Override
	public boolean hasStopped() {
		if (this.hitWall == true || this.batteryLevel <= 0) { // ||this.isValidPosition == false
			return true;
		}
		return false;
	}

	/**
	 * Detect the distance to obstacle by splitting it into three situations.
	 * First situation is that the robot is facing a obstacle and is not at the exit position, so return the calculated direction.
	 * Second situation is that the robot is at the exit position but is not facing the exit direction, so return the calculated direction.
	 * Third situation is that the robot is facing the exit direction, so and return Integer.Max_Value.
	 */
	@Override
	public int distanceToObstacle(Direction direction) throws UnsupportedOperationException {
//		if (this.controller.currentState != controller.states[2]) {
//			return -100000;
//		}
		if (this.hasOperationalSensor(direction) == false) {
			throw new UnsupportedOperationException("The robot does not have an operational sensor for this direction");
		}
		int x = this.statePlaying.getCurrentPosition()[0];
		int y = this.statePlaying.getCurrentPosition()[1];
		CardinalDirection currentDir = this.getCurrentDirection();
		CardinalDirection newDir = null;
		if (currentDir == CardinalDirection.East || currentDir == CardinalDirection.West) {
			//System.out.println("east and west");
			newDir = this.eastAndWestFindNewDirection(direction);
		}
		else if (currentDir == CardinalDirection.North || currentDir == CardinalDirection.South) {
			newDir = this.northAndSouthfindNewDirection(direction);
		}

		if (direction != Direction.FORWARD) {
			assert currentDir != newDir;   //assert() statement to check the cardinal direction with the given relative direction
		}
		int distance = 0;
		int curx = x, cury = y;
		while (this.maze.isValidPosition(curx, cury)) {
			if (this.maze.hasWall(curx, cury, newDir) == true) {
				this.batteryLevel --;
				this.switchFromPlayingToLosingIfNecessary(x, y);
				return distance;
			}
			switch(newDir) {
			case East:
				curx ++;
				break;
			case West:
				curx --;
				break;
			case North:
				cury --;
				break;
			case South:
				cury++;
				break;
			}
			assert curx != x || cury != y; //Use assert() statement to check whether the current position is moving to detect obstacles.
			distance ++;
		}
		this.batteryLevel --;
		//System.out.println("distance called method to switch to lose");
		this.switchFromPlayingToLosingIfNecessary(x, y);
		return Integer.MAX_VALUE;
	}

	/**
	 * A helper function to determine whether the game is failed.
	 * Check the current battery level, check whether the current position is at the exit//
	 * and check whether the robot is facing the exit direction.
	 * If the game is failed, operate the controller to switch from the StatePlaying to StateWinning.
	 * @param x
	 * @param y
	 */
	public void switchFromPlayingToLosingIfNecessary(int x, int y) {
		Floorplan floorplan = this.maze.getFloorplan();
		if (this.batteryLevel <= 0) {
			System.out.println("--------battery level < 0--------");
			if ((floorplan.isExitPosition(x, y) == false) || (floorplan.isExitPosition(x, y) == true && this.canSeeThroughTheExitIntoEternity(Robot.Direction.FORWARD) == false)) {
				System.out.println("here");
				this.playAnimationActivity.switchFromPlayingToLosing(this.odoMeter, this.batteryLevel);
			}
		}
	}

	/**
	 * A helper function used in distanceToObstacle() when current cardinal direction is East or West.
	 * Find the cardinal direction of the robot's relative direction, e.g., if robot's cardinal direction is East
	 * and relative direction is Robot.RIGHT,then the new cardinal direction is North.
	 * @param direction
	 * @return The cardinal direction corresponds to robot's relative direction.
	 */
	private CardinalDirection eastAndWestFindNewDirection(Direction direction) {
		CardinalDirection currentDir = this.getCurrentDirection();
		CardinalDirection newDir = null;
		switch (currentDir) {
		case East:
			if (direction == Direction.LEFT) {
				newDir = CardinalDirection.South;
			}
			if (direction == Direction.RIGHT) {
				newDir = CardinalDirection.North;
			}
			if (direction == Direction.FORWARD) {
				newDir = CardinalDirection.East;
			}
			if (direction == Direction.BACKWARD) {
				newDir = CardinalDirection.West;
			}
			break;
		case West:
			if (direction == Direction.LEFT) {
				newDir = CardinalDirection.North;
			}
			if (direction == Direction.RIGHT) {
				newDir = CardinalDirection.South;
			}
			if (direction == Direction.FORWARD) {
				newDir = CardinalDirection.West;
			}
			if (direction == Direction.BACKWARD) {
				newDir = CardinalDirection.East;
			}
			break;
		}
		return newDir;
	}

	/**
	 * A helper function used in distanceToObstacle() when current cardinal direction is north or south.
	 * Find the cardinal direction of the robot's relative direction, e.g., if robot's cardinal direction is East
	 * and relative direction is Robot.RIGHT,then the new cardinal direction is North.
	 * @param direction
	 * @return The cardinal direction corresponds to robot's relative direction.
	 */
	private CardinalDirection northAndSouthfindNewDirection(Direction direction) {
		CardinalDirection currentDir = this.getCurrentDirection();
		CardinalDirection newDir = null;
		switch (currentDir) {
		case North:
			if (direction == Direction.LEFT) {
				newDir = CardinalDirection.East;
			}
			if (direction == Direction.RIGHT) {
				newDir = CardinalDirection.West;
			}
			if (direction == Direction.FORWARD) {
				newDir = CardinalDirection.North;
			}
			if (direction == Direction.BACKWARD) {
				newDir = CardinalDirection.South;
			}
			break;
		case South:
			if (direction == Direction.LEFT) {
				newDir = CardinalDirection.West;
			}
			if (direction == Direction.RIGHT) {
				newDir = CardinalDirection.East;
			}
			if (direction == Direction.FORWARD) {
				newDir = CardinalDirection.South;
			}
			if (direction == Direction.BACKWARD) {
				newDir = CardinalDirection.North;
			}
			break;
		}
		return newDir;
	}

	/**
	 * Return true/false of the corresponding sensor to indicate whether the sensor is functional or not
	 */
	@Override
	public boolean hasOperationalSensor(Direction direction) {
		switch (direction) {
		case FORWARD:
			return this.forwardSensor;
		case BACKWARD:
			return this.backwardSensor;
		case LEFT:
			return this.leftSensor;
		default:
			return this.rightSensor;
		}

	}
	/**
	 * Trigger sensor failure by making the sensor flag false
	 */
	@Override
	public void triggerSensorFailure(Direction direction) {
		switch (direction) {
		case FORWARD:
			this.forwardSensor = false;
			break;
		case BACKWARD:
			this.backwardSensor = false;
			break;
		case LEFT:
			this.leftSensor = false;
			break;
		case RIGHT:
			this.rightSensor = false;
			break;
		}
	}

	/**
	 * Repair failed sensors of the robot.
	 * Here, since the robot has sensors in all directions, it should always return true.
	 */
	@Override
	public boolean repairFailedSensor(Direction direction) {
		switch (direction) {
		case FORWARD:
			this.forwardSensor = true;
			break;
		case BACKWARD:
			this.backwardSensor = true;
			break;
		case LEFT:
			this.leftSensor = true;
			break;
		case RIGHT:
			this.rightSensor = true;
			break;
		}
		return true;
	}
	/**
	 * Rotate the robot according to the relative direction.
	 */
	@Override
	public void rotate(Turn turn) {
//		if (this.controller.currentState != controller.states[2]) {
//			return;
//		}
		CardinalDirection initialDir = this.getCurrentDirection();
		if (this.hasStopped() == false) {
			switch (turn) {
			case LEFT:
				this.statePlaying.keyDown(UserInput.Left, 0);
				this.batteryLevel = this.batteryLevel - 3;
				break;
			case RIGHT:
				this.statePlaying.keyDown(UserInput.Right, 0);
				this.batteryLevel = this.batteryLevel - 3;
				break;
			case AROUND:
				this.statePlaying.keyDown(UserInput.Left, 0);
				this.statePlaying.keyDown(UserInput.Left, 0);
				this.batteryLevel = this.batteryLevel - 6;
				break;
			}
		}
		CardinalDirection newDir = this.getCurrentDirection();
		//assert initialDir != newDir; //Use assert() method to check whether cardinal direction after rotating is different from the original cardinal direction.
		int x = this.statePlaying.getCurrentPosition()[0];
		int y = this.statePlaying.getCurrentPosition()[1];
		Floorplan floorplan = this.maze.getFloorplan();
		Direction [] directions= {Direction.LEFT, Direction.RIGHT, Direction.BACKWARD};
		//account for situation when robot at the exit and does not have a forward sensor
		if (this.batteryLevel >= 0 && floorplan.isExitPosition(x, y) && this.canSeeThroughTheExitIntoEternity(Robot.Direction.FORWARD)) {
			this.playAnimationActivity.switchFromPlayingToWinning(this.odoMeter, this.batteryLevel);
		}
		else {
			this.switchFromPlayingToLosingIfNecessary(x, y);
		}
	}
	/**
	 * Move the robot a certain distance.
	 * If the robot encounter a wall during the process, hasStopped will be changed accordingly.
	 */
	@Override
	public void move(int distance, boolean manual) {
//		if (this.controller.currentState != controller.states[2]) {
//			return;
//		}
		int steps = 0;
		CardinalDirection currentDir = this.getCurrentDirection();
		int[] initialPos = this.statePlaying.getCurrentPosition();
		while (this.hasStopped() == false && steps < distance) {
			int x = this.statePlaying.getCurrentPosition()[0];
			int y = this.statePlaying.getCurrentPosition()[1];
			if (this.maze.hasWall(x, y, currentDir) && manual == false) {
				this.hitWall = true;
				break;
			}
			else if (this.maze.hasWall(x, y, currentDir) && manual == true) {
				break;
			}

			else {
				this.statePlaying.keyDown(UserInput.Up, 0);
				steps++;
				this.batteryLevel = this.batteryLevel - 5;
				this.odoMeter++;
			}
		}
		int x = this.statePlaying.getCurrentPosition()[0];
		int y = this.statePlaying.getCurrentPosition()[1];
//		if (steps != 0) {
//			assert initialPos[0] != x || initialPos[1] != y; //Use assert() method to check whether the position after moving is different from the original position.
//		}
		Floorplan floorplan = this.maze.getFloorplan();
		if (this.batteryLevel >= 0 && floorplan.isExitPosition(x, y) && this.canSeeThroughTheExitIntoEternity(Robot.Direction.FORWARD)) {
			this.playAnimationActivity.switchFromPlayingToWinning(this.odoMeter, this.batteryLevel);
		}
		else{
			this.switchFromPlayingToLosingIfNecessary(x, y);
		}
	}
	/**
	 * Robot will jump to the position even if there is a wall. An exception is thrown if jump will lead to invalid position
	 * Battery level should decrease by 50; odometer increases by one
	 */
	@Override
	public void jump() throws Exception {
//		if (this.controller.currentState != controller.states[2]) {
//			return;
//		}
		CardinalDirection currentDir = this.getCurrentDirection();
		int[] initialPos = this.statePlaying.getCurrentPosition();
		int x = this.statePlaying.getCurrentPosition()[0];
		int y = this.statePlaying.getCurrentPosition()[1];
		switch (currentDir) {
		case East:
			x++;
			break;
		case West:
			x--;
			break;
		case North:
			y--;
			break;
		case South:
			y++;
		}
		if (this.maze.isValidPosition(x, y) == false) {
			throw new Exception("The robot cannot jump here; otherwise. It will jump outside of the maze");
		}
		else {
			this.statePlaying.keyDown(UserInput.Jump, 0);
			this.batteryLevel = this.batteryLevel - 50;
			this.odoMeter++;
		}
		int newx = this.statePlaying.getCurrentPosition()[0];
		int newy = this.statePlaying.getCurrentPosition()[1];
		//assert initialPos[0] != newx || initialPos[1] != newy; //Use assert() method to check whether position after jumping is different from the original position.
		Floorplan floorplan = this.maze.getFloorplan();
		if (this.batteryLevel >= 0 && floorplan.isExitPosition(newx, newy) && this.canSeeThroughTheExitIntoEternity(Robot.Direction.FORWARD)) {
			this.playAnimationActivity.switchFromPlayingToWinning(this.odoMeter, this.batteryLevel);
		}
		else {
			this.switchFromPlayingToLosingIfNecessary(newx, newy);
		}
	}


}
