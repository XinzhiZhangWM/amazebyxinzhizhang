package edu.wm.cs.cs301.XinzhiZhang.gui;

import edu.wm.cs.cs301.XinzhiZhang.gui.Robot.Direction;

/**
 * Create a class to implement the Runnable so that
 * each sensor can have a separate thread to operate the trigger/repair process.
 * @author Lulu Zhang
 *
 */
public class RobotSensorThread implements Runnable {
	Robot robot;
	Direction direction;
	RobotDriver driver;
	
	public RobotSensorThread(Robot robot, RobotDriver driver, Direction direction) {
		this.robot = robot;
		this.direction = direction;
		this.driver = driver;
	}
	
	public void run() {
		while (true) {
			if (robot.hasOperationalSensor(direction)) {
				robot.triggerSensorFailure(direction);
			}
			else {
				robot.repairFailedSensor(direction);
			}
			
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e1) {
				robot.repairFailedSensor(direction);
				return;
			}
		}
	}
}
