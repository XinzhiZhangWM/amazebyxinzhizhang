package edu.wm.cs.cs301.XinzhiZhang.gui;

import edu.wm.cs.cs301.XinzhiZhang.generation.Maze;

/**
 * A singleton class to pass the maze configurations from GeneratingActivity
 * to PlayAnimationActivity and PlayManuallyActivity
 */
public class MazeHolder {
    private static Maze maze;

    /**
     * Receive maze from GeneratingActivity
     * @param maze
     */
    public static void setMaze(Maze maze){
        MazeHolder.maze = maze;
    }

    /**
     * Pass maze to PlayAnimationActivity or PlayManuallyActivity
     * @return
     */
    public static Maze getMaze(){
        return MazeHolder.maze;
    }
}
