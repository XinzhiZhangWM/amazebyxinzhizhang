package edu.wm.cs.cs301.XinzhiZhang.gui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Canvas;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ToggleButton;

import edu.wm.cs.cs301.XinzhiZhang.R;
import edu.wm.cs.cs301.XinzhiZhang.generation.Maze;

import static edu.wm.cs.cs301.XinzhiZhang.R.raw.button_click;

public class PlayAnimationActivity extends AppCompatActivity {
    private float batteryLevel;
    private int pathLength;
    private int shortestPathLength;
    private Bundle bundle;
    private MazePanel panel;
    private Canvas canvas;
    private Maze maze;
    private Robot robot;
    private RobotDriver robotDriver;
    private StatePlaying statePlaying;
    private Handler handler = new Handler();
    private MediaPlayer mediaPlayer;
    private MediaPlayer backgroundMediaPlayer;
    //Buttons
    private ToggleButton toggleWalls;
    private ToggleButton toggleMap;
    private ToggleButton toggleSolution;
    private Button startButton;
    private ToggleButton leftButton;
    private ToggleButton rightButton;
    private ToggleButton forwardButton;
    private ToggleButton backwardButton;
    //Flags
    private boolean pauseFlag = false;
    private boolean started = false;
    //Threads
    private Thread driverThread;
    private Thread leftSensorThread;
    private Thread rightSensorThread;
    private Thread forwardSensorThread;
    private Thread backwardSensorThread;
    //Tags
    private final static String batteryLevelMessage = "PlayActivity.batteryLevel";
    private final static String pathLengthMessage = "PlayActivity.pathLength";
    private final static String shortestPathMessage = "PlayActivity.shortestPath";
    private final String logTag = "edu.wm.cs.cs301.XinzhiZhang.gui.PlayAnimationActivity";

    /**
     * Called once play animation activity is started
     * Display the play animation screen
     * Set the default state of showing walls, map, and solution to be false by setting the state of the toggle buttons
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_animation);
        setButtons();
        setRobotandRobotDriver();
        setMazeAndMazePanel();
        setStatePlaying();
        ((BasicRobot) robot).setPlayAnimationActivity(this);
    }

    /**
     * A helper function to set the buttons
     */
    private void setButtons(){
        toggleWalls= findViewById(R.id.show_walls_button);
        toggleWalls.setChecked(true);
        toggleMap = findViewById(R.id.show_full_maze_button);
        toggleMap.setChecked(true);
        toggleSolution = findViewById(R.id.show_solution_button);
        toggleSolution.setChecked(true);
        startButton = findViewById(R.id.start_button);
        leftButton = findViewById(R.id.left_sensor_toggle_button);
        rightButton = findViewById(R.id.right_sensor_toggle_button);
        forwardButton = findViewById(R.id.forward_sensor_toggle_button);
        backwardButton = findViewById(R.id.backward_sensor_toggle_button);
    }

    /**
     * A helper function to initialize the robot and driver
     */
    private void setRobotandRobotDriver(){
        Intent intent = getIntent();
        String driver = intent.getStringExtra("GeneratingActivity.Driver");
        switch (driver){
            case "Wall Follower":
                robot = new BasicRobot();
                robotDriver = new WallFollower();
                robotDriver.setRobot(robot);
                break;
            case "Wizard":
                robot = new BasicRobot();
                robotDriver = new Wizard();
                robotDriver.setRobot(robot);
                break;
        }
    }

    /**
     * A helper function to set the maze panel and get the maze passed from generating activity
     */
    private void setMazeAndMazePanel(){
        maze = MazeHolder.getMaze();
        panel = findViewById(R.id.maze_panel);
    }

    /**
     * A helper function to pass the robot and driver to stateplaying class
     */
    private void setStatePlaying() {
        statePlaying = new StatePlaying();
        statePlaying.setMazeConfiguration(maze);
        statePlaying.setHasStopped(robot.hasStopped());
        statePlaying.setRobot(robot);
        statePlaying.setRobotDriver(robotDriver);
        statePlaying.setHandler(handler);
        statePlaying.start(panel);
    }

    /**
     * A helper function to obtain the shortest path length of the maze
     */
    private void setShortestPathLength(){
        int [] startPos = maze.getStartingPosition();
        this.shortestPathLength = maze.getDistanceToExit(startPos[0], startPos[1]);
        if (pathLength < shortestPathLength){
            shortestPathLength = pathLength;
        }
    }

    /**
     * Initiate a media player to play the background music.
     */
    private void playPlayingSound(){
        if (backgroundMediaPlayer != null) {
            backgroundMediaPlayer.reset();
            backgroundMediaPlayer.release();
            backgroundMediaPlayer = null;
        }
        backgroundMediaPlayer = MediaPlayer.create(PlayAnimationActivity.this, R.raw.playing_music);
        backgroundMediaPlayer.setVolume(20, 20);
        backgroundMediaPlayer.setLooping(true);
        backgroundMediaPlayer.start();
        if (backgroundMediaPlayer.isPlaying()){
            Log.v(logTag, "background media play has started");
        }
    }

    /**
     * Return the maze
     * @return
     */
    public Maze getMaze(){
        return maze;
    }

//    public void draw(){
//        panel = new MazePanel(this);
//        panel.invalidate();
//        panel.draw(canvas);
//    }

    /**
     * Called once click the zoom-in button
     * @param view: zoom-in button
     */
    public void zoomIn(View view){
        playButtonClickSound();
        statePlaying.keyDown(Constants.UserInput.ZoomIn, 0);
        Log.v(logTag, "zoom in");
    }

    /**
     * Called once click the zoom-out button
     * @param view: zoom-out button
     */
    public void zoomOut(View view){
        playButtonClickSound();
        statePlaying.keyDown(Constants.UserInput.ZoomOut, 0);
        Log.v(logTag, "zoom out");
    }

    /**
     * Called once click the toggle button to show show/hide the walls
     * Show the wall if the button is checked
     * Hide the wall otherwise.
     * @param view: toggle button
     */
    public void toggleShowWalls(View view){
        playButtonClickSound();
        statePlaying.keyDown(Constants.UserInput.ToggleLocalMap, 0);
        boolean toggleButtonIsChecked = toggleWalls.isChecked();
        if (toggleButtonIsChecked) {
            Log.v(logTag, "show walls");
        }
        else{
            Log.v(logTag, "hide walls");
        }
    }

    /**
     * Called once click the toggle button to show/hide the map
     * Show the map if the button is checked
     * Hide the map otherwise.
     * @param view: toggle button
     */
    public void toggleShowFullMaze(View view){
        playButtonClickSound();
        statePlaying.keyDown(Constants.UserInput.ToggleFullMap, 0);
        boolean toggleButtonIsChecked = toggleMap.isChecked();
        if (toggleButtonIsChecked) {
            Log.v(logTag, "show the whole maze");
        }
        else{
            Log.v(logTag, "hide the whole maze");
        }
    }

    /**
     * Called once click the toggle button to show/hide solution
     * Show the solution if the button is checked
     * Hide the solution otherwise.
     * @param view:toggle button
     */
    public void toggleShowSolution(View view){
        playButtonClickSound();
        statePlaying.keyDown(Constants.UserInput.ToggleSolution, 0);
        boolean toggleButtonIsChecked = toggleSolution.isChecked();
        if (toggleButtonIsChecked) {
            Log.v(logTag, "show the solution");
        }
        else{
            Log.v(logTag, "hide the solution");
        }
//        toggleSolution.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
//                if (isChecked) {
//                    Toast.makeText(getApplicationContext(), "show the solution", Toast.LENGTH_SHORT).show();
//                }
//                else{
//                    Toast.makeText(getApplicationContext(), "hide the solution", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
    }



    /**
     * Called once click the start button to start the exploration
     * @param view: start button
     */
    public void start(View view){
        playButtonClickSound();
        playPlayingSound();
        if(robotDriver != null && driverThread == null && started == false){
            started = true;
            driverThread = new Thread(robotDriver);
            driverThread.start();
            startButton.setText(R.string.Resume);
            Log.v(logTag, "start robot driver");
        }
        else if (driverThread == null && pauseFlag && started){
            robotDriver.setHasStopped(false);
            pauseFlag = false;
            driverThread = new Thread(robotDriver);
            driverThread.start();
            Log.v(logTag, "resume game");
        }
    }

//    private Runnable updateScreen = new Runnable() {
//        @Override
//        public void run() {
//            statePlaying.draw();
//            handler.postDelayed(this, 1000);
//        }
//    };

    /**
     * Called once click the pause button to pause the exploration
     * @param view: pause button
     */
    public void pause(View view){
        playButtonClickSound();
        if (driverThread != null && pauseFlag == false && started ){
            robotDriver.setHasStopped(true);
            pauseFlag = true;
            driverThread = null;
            Log.v(logTag, "pause game");
        }
    }

    /**
     * Called when click the button to trigger failure of left sensor
     * start a separate thread
     * @param view
     */
    public void triggerLeftSensorThread(View view){
        playButtonClickSound();
        if (leftSensorThread== null) {
            leftSensorThread = new Thread(new RobotSensorThread(robot, robotDriver, Robot.Direction.LEFT));
            leftSensorThread.start();
            leftButton.setText(R.string.fail_left_sensor);
            Log.v(logTag, "left sensor thread started");
        } else {
            leftSensorThread.interrupt();
            leftSensorThread = null;
            leftButton.setText(R.string.repair_left_sensor);
            Log.v(logTag, "left sensor stopped");
        }

    }

    /**
     * Called when click the button to trigger failure of right sensor
     * start a separate thread
     * @param view
     */
    public void triggerRightSensorThread(View view){
        playButtonClickSound();
        if (rightSensorThread== null) {
            rightSensorThread = new Thread(new RobotSensorThread(robot, robotDriver, Robot.Direction.RIGHT));
            rightSensorThread.start();
            rightButton.setText(R.string.fail_right_sensor);
            Log.v(logTag, "right sensor thread started");
        } else {
            rightSensorThread.interrupt();
            rightSensorThread = null;
            rightButton.setText(R.string.repair_right_sensor);
            Log.v(logTag, "right sensor stopped");
        }
    }

    /**
     * Called when click the button to trigger failure of forward sensor
     * start a separate thread
     * @param view
     */
    public void triggerForwardSensorThread(View view){
        playButtonClickSound();
        if (forwardSensorThread== null) {
            forwardSensorThread = new Thread(new RobotSensorThread(robot, robotDriver, Robot.Direction.FORWARD));
            forwardSensorThread.start();
            forwardButton.setText(R.string.fail_forward_sensor);
            Log.v(logTag, "forward sensor thread started");
        } else {
            forwardSensorThread.interrupt();
            forwardSensorThread = null;
            forwardButton.setText(R.string.repair_forward_sensor);
            Log.v(logTag, "forward sensor stopped");
        }
    }

    /**
     * Called when click the button to trigger failure of backward sensor
     * start a separate thread
     * @param view
     */
    public void triggerBackwardSensorThread(View view){
        playButtonClickSound();
        if (backwardSensorThread== null) {
            backwardSensorThread = new Thread(new RobotSensorThread(robot, robotDriver, Robot.Direction.BACKWARD));
            backwardSensorThread.start();
            backwardButton.setText(R.string.fail_backward_sensor);
            Log.v(logTag, "backward sensor thread started");
        } else {
            backwardSensorThread.interrupt();
            backwardSensorThread = null;
            backwardButton.setText(R.string.repair_backward_sensor);
            Log.v(logTag, "backward sensor stopped");
        }
    }
    /**
     * Called once click the back button
     * Return to the state tile state
     * @param view: back button
     */
    public void switchToTitle(View view){
        playButtonClickSound();
        backgroundMediaPlayer.stop();
        robotDriver.setHasStopped(true);
        if (driverThread != null){
            driverThread.interrupt();
            driverThread = null;
        }
        Intent intent = new Intent(getApplicationContext(), AMazeActivity.class);
        startActivity(intent);
        Log.v(logTag, "switch to title from state PlayAnimation");
    }

    /**
     * Create a bundle to store the information battery level, path length, and the shortest path length,
     * which can be used to pass to an intent so that information can be passed to winning/losing state
     */
    private void setWinningLosingInformationBundle(){
        setShortestPathLength();
        bundle = new Bundle();
        bundle.putFloat(batteryLevelMessage, batteryLevel);
        bundle.putInt(pathLengthMessage, pathLength);
        bundle.putInt(shortestPathMessage, shortestPathLength);
        Log.v(logTag, "game information saved to bundle to be passed to winning or losing activity");
    }

    /**
     * A place holder button for project 6 only, helping to switch to winning state
     * @param
     */
    public void switchFromPlayingToWinning(int pathLength, float batteryLevel){
        this.batteryLevel = 3000 - batteryLevel;
        this.pathLength = pathLength;
        this.setWinningLosingInformationBundle();
        backgroundMediaPlayer.stop();
        Intent intent = new Intent(getApplicationContext(), WinningActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        Log.v(logTag, "switch to state winning from state PlayAnimation");
    }

    /**
     * A place holder button for project 6 only, helping to switch to winning state
     * @param
     */
    public void switchFromPlayingToLosing(int pathLength, float batteryLevel){
        this.batteryLevel = 3000 - batteryLevel;
        this.pathLength = pathLength;
        this.setWinningLosingInformationBundle();
        backgroundMediaPlayer.stop();
        Intent intent = new Intent(getApplicationContext(),  LosingActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        Log.v(logTag, "switch to state losing from state PlayAnimation");
    }

    private void playButtonClickSound(){
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        mediaPlayer = MediaPlayer.create(PlayAnimationActivity.this, button_click);
        mediaPlayer.setVolume(1000, 1000);
        mediaPlayer.start();
        if (mediaPlayer.isPlaying()){
            Log.v(logTag, "button click media play has started");
        }
    }
}
