package edu.wm.cs.cs301.XinzhiZhang.gui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import edu.wm.cs.cs301.XinzhiZhang.R;
import edu.wm.cs.cs301.XinzhiZhang.generation.Factory;
import edu.wm.cs.cs301.XinzhiZhang.generation.Maze;
import edu.wm.cs.cs301.XinzhiZhang.generation.MazeFactory;
import edu.wm.cs.cs301.XinzhiZhang.generation.Order;

import static edu.wm.cs.cs301.XinzhiZhang.R.raw.button_click;

public class GeneratingActivity extends AppCompatActivity implements Order {
    private ProgressBar progressBar;
    private TextView textView;
    private int skillLevel;
    private String mazeGeneration;
    private String driver;
    private boolean revisit;
    private int seed;
    private Factory factory;
    private boolean started = false;
    private boolean perfect;
    private int percentage;
    private Builder builder;
    private Handler handler;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private MediaPlayer mediaPlayer;
    private static final String driverMessage = "GeneratingActivity.Driver";
    private final String logTag = "edu.wm.cs.cs301.XinzhiZhang.gui.GeneratingActivity";
    private static final String preferenceFile = "preferenceFile";


    /**
     * Called once the application switch to maze generating state
     * Set the layout of the app to be the state generating layout
     * Call updateProgressBar to update progress bar and start new thread
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_generating);
        init();
        getUserInput();
        if (revisit){
            Log.v(logTag, "user chose to revisit a maze");
            revisit();
        }
        else{
            Log.v(logTag, "user chose to explore a maze");
            explore();
        }
    }

    /**
     * A helper function to set the initial attribute state
     */
    private void init(){
        started = false;
        perfect = false;
        percentage = 0;
        progressBar = findViewById(R.id.maze_generation_progress_bar);
        textView = findViewById(R.id.percentage);
        handler = new Handler(){
            @Override
            public void handleMessage(Message msg){
                int percentage = msg.what;
                progressBar.setProgress(percentage);
                textView.setText(percentage + "%");
            }
        };
        sharedPreferences = getSharedPreferences(preferenceFile, this.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    /**
     * Get the information of skill level, maze generator, and driver passed from AMazeActivity
     */
    private void getUserInput(){
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        skillLevel = bundle.getInt("AMazeActivity.SkillLevel");
        mazeGeneration = bundle.getString("AMazeActivity.MazeGeneration");
        switch (mazeGeneration){
            case "DFS":
                builder = Builder.DFS;
                break;
            case "Prim":
                builder = Builder.Prim;
                break;
            case "Eller":
                builder = Builder.Eller;
                break;
        }
        driver = bundle.getString("AMazeActivity.Driver");
        if(driver.equals("Wall Follower")){
            this.perfect = true;
        }
        revisit = bundle.getBoolean("AMazeActivity.revisitMaze");
        Log.v(logTag, "skill level = " + skillLevel);
        Log.v(logTag, "maze generator is " + mazeGeneration);
        Log.v(logTag, "driver is " + driver);
    }

    /**
     * Return the skill level of the maze;
     * @return skill level
     */
    @Override
    public int getSkillLevel(){
        return this.skillLevel;
    }

    /**
     * Return the maze generator
     * @return maze generator
     */
    @Override
    public Builder getBuilder(){
        return builder;
    }

    /**
     * Return the boolean value of whether the maze is perfect
     * @return boolean value
     */
    @Override
    public boolean isPerfect(){
        return perfect;
    }

    /**
     * A helper function to start generating a maze from the maze factory
     * Called when user chose to explore a new maze
     * Save the skill level and maze generator to the preference files
     */
    private void explore(){
        started = true;
        factory = new MazeFactory();
        factory.order(this);
        seed = ((MazeFactory) factory).getSeed();
        savePreferences();
    }

    /**
     * A helper function to load a maze from file
     * Called when user chose to revisit a maze
     * Use mazeGeneration and skillLevel as keys to find corresponding seed value stored in preference file
     */
    private void revisit(){
        String key = mazeGeneration + skillLevel;
        if (sharedPreferences != null && sharedPreferences.contains(key)) {
            Log.v(logTag, "shared preference contains the key");
            seed = sharedPreferences.getInt(key, 1);
            factory = new MazeFactory(true, seed);
            factory.order(this);
        }
        else{
            Toast.makeText(this, "No such maze has generated before. A new maze will be explored.", Toast.LENGTH_LONG).show();
            Log.v(logTag, "shared preference does not contain the key");
            explore();
        }
    }

    /**
     * A helper function to store key, i.e., mazeGeneration + skillLevel and corresponding seed value
     */
    private void savePreferences(){
        String key = mazeGeneration + skillLevel;
        editor.putInt(key, seed);
        editor.apply();
    }

    /**
     * Update the UI thread of the percentage of generated maze while loading the maze
     * @param percentage: current percentage of job completion
     */
    @Override
    public void updateProgress(int percentage){
        if (this.percentage < percentage && percentage <= 100) {
            this.percentage = percentage;
            handler.sendMessage(Message.obtain(handler, this.percentage));
            Log.v(logTag, "handler passed percentage to GeneratingActivity");
        }
    }

    /**
     * Switch to PlayManuallyActivity or PlayAnimationActivity after generating a maze
     * @param maze
     */
    @Override
    public void deliver(Maze maze){
        MazeHolder.setMaze(maze);
        Intent intent;
        if (driver.equals("Manual")){
            Log.v(logTag, "manual is passed to state generating");
            intent = new Intent(getApplicationContext(), PlayManuallyActivity.class);
        }
        else{
            Log.v(logTag, "driver is passed to state generating");
            intent = new Intent(getApplicationContext(),  PlayAnimationActivity.class);
            intent.putExtra(driverMessage, driver);
        }
        startActivity(intent);
    }

    /**
     * Called once clicking the back button on the screen
     * Interrupt the background thread and switch to state title screen
     * @param view: the back button
     */
    public void switchToTitle(View view){
        playButtonClickSound();
        factory.cancel();
        Intent intent = new Intent(getApplicationContext(), AMazeActivity.class);
        startActivity(intent);
        Log.v(logTag, "switch to title from state generating");
    }

    private void playButtonClickSound(){
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        mediaPlayer = MediaPlayer.create(GeneratingActivity.this, button_click);
        mediaPlayer.setVolume(1000, 1000);
        mediaPlayer.start();
        if (mediaPlayer.isPlaying()){
            Log.v(logTag, "button click media play has started");
        }
    }
}
