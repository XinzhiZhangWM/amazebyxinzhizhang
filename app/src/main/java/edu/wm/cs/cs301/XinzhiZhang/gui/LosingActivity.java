package edu.wm.cs.cs301.XinzhiZhang.gui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import edu.wm.cs.cs301.XinzhiZhang.R;

import static edu.wm.cs.cs301.XinzhiZhang.R.raw.button_click;

public class LosingActivity extends AppCompatActivity {
    private float batteryLevel;
    private int pathLength;
    private int shortestPath;
    private MediaPlayer mediaPlayer;
    private final String logTag = "edu.wm.cs.cs301.XinzhiZhang.gui.LosingActivity";

    /**
     * Called once the losing activity is started. Display the losing state layout on the screen
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_losing);
        getPlayingInformation();
        playLosingSound();
    }

    /**
     * Get information passed from play animation/play manually state of battery level, path length, and shortest path.
     * Display the information to corresponding text views on the screen
     */
    private void getPlayingInformation(){
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        TextView batteryLevelText = findViewById(R.id.battery_level);
        TextView pathLengthText = findViewById(R.id.path_length);
        TextView shortestPathText = findViewById(R.id.shortest_path);
        batteryLevel = bundle.getFloat("PlayActivity.batteryLevel");
        batteryLevelText.setText(Float.toString(batteryLevel));
        pathLength = bundle.getInt("PlayActivity.pathLength");
        pathLengthText.setText(Integer.toString(pathLength));
        shortestPath = bundle.getInt("PlayActivity.shortestPath");
        shortestPathText.setText(Integer.toString(shortestPath));
    }

    private void playLosingSound(){
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        mediaPlayer = MediaPlayer.create(LosingActivity.this, R.raw.losing);
        mediaPlayer.setVolume(100, 100);
        mediaPlayer.start();
        System.out.println(mediaPlayer.isPlaying());
    }

    /**
     * Called once click the back button to switch the titel screen
     * @param view: back button
     */
    public void switchToTitle(View view){
        playButtonClickSound();
        Intent intent = new Intent(getApplicationContext(), AMazeActivity.class);
        startActivity(intent);
        Log.v(logTag, "switch to title from state losing");
    }

    private void playButtonClickSound(){
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        mediaPlayer = MediaPlayer.create(LosingActivity.this, button_click);
        mediaPlayer.setVolume(1000, 1000);
        mediaPlayer.start();
        System.out.println(mediaPlayer.isPlaying());
    }
}
