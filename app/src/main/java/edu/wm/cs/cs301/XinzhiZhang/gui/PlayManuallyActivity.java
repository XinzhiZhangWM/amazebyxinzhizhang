package edu.wm.cs.cs301.XinzhiZhang.gui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Canvas;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ToggleButton;

import edu.wm.cs.cs301.XinzhiZhang.R;
import edu.wm.cs.cs301.XinzhiZhang.generation.Maze;

import static edu.wm.cs.cs301.XinzhiZhang.R.raw.button_click;

public class PlayManuallyActivity extends AppCompatActivity {
    private float batteryLevel;
    private int pathLength;
    private int shortestPathLength;
    private Bundle bundle;
    private ToggleButton toggleWalls;
    private ToggleButton toggleMap;
    private ToggleButton toggleSolution;
    private MazePanel panel;
    private Canvas canvas;
    private Maze maze;
    private StatePlaying statePlaying;
    private Handler handler = new Handler();
    private MediaPlayer mediaPlayer;
    private MediaPlayer backgroundMediaPlayer;
    private final static String batteryLevelMessage = "PlayActivity.batteryLevel";
    private final static String pathLengthMessage = "PlayActivity.pathLength";
    private final static String shortestPathMessage = "PlayActivity.shortestPath";
    private final String logTag = "edu.wm.cs.cs301.XinzhiZhang.gui.PlayManuallyActivity";

    /**
     * Called once play animation activity is started
     * Display the play animation screen
     * Set the default state of showing walls, map, and solution to be false by setting the state of the toggle buttons
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_manually);
        toggleWalls= findViewById(R.id.show_walls_button);
        toggleWalls.setChecked(false);
        toggleMap = findViewById(R.id.show_full_maze_button);
        toggleMap.setChecked(false);
        toggleSolution = findViewById(R.id.show_solution_button);
        toggleSolution.setChecked(false);
        setMazeAndMazePanel();
        setStatePlaying();
        setShortestPathLength();
        playPlayingSound();
    }

    /**
     * A helper function to set the maze panel and get the maze passed from generating activity
     */
    private void setMazeAndMazePanel(){
        maze = MazeHolder.getMaze();
        panel = findViewById(R.id.maze_panel);
    }

    /**
     * A helper function to pass the robot and driver to stateplaying class
     */
    private void setStatePlaying(){
        statePlaying = new StatePlaying();
        statePlaying.setMazeConfiguration(maze);
        statePlaying.setHasStopped(false);
        statePlaying.setRobot(null);
        statePlaying.setRobotDriver(null);
        statePlaying.setPlayingState(this);
        statePlaying.setHandler(handler);
        statePlaying.start(panel);
    }

    /**
     * A helper function to obtain the shortest path length of the maze
     */
    private void setShortestPathLength(){
        int [] startPos = maze.getStartingPosition();
        shortestPathLength = maze.getDistanceToExit(startPos[0], startPos[1]);
    }

    /**
     * Initiate a media player to play the background music.
     */
    private void playPlayingSound(){
        if (backgroundMediaPlayer != null) {
            backgroundMediaPlayer.reset();
            backgroundMediaPlayer.release();
            backgroundMediaPlayer = null;
        }
        backgroundMediaPlayer = MediaPlayer.create(PlayManuallyActivity.this, R.raw.playing_music);
        backgroundMediaPlayer.setVolume(20, 20);
        backgroundMediaPlayer.setLooping(true);
        backgroundMediaPlayer.start();
        if (backgroundMediaPlayer.isPlaying()){
            Log.v(logTag, "background media play has started");
        }
    }

    /**
     * Called once click the zoom-in button
     * @param view: zoom-in button
     */
    public void zoomIn(View view){
        playButtonClickSound();
        statePlaying.keyDown(Constants.UserInput.ZoomIn, 0);
        Log.v(logTag, "zoom in");
    }

    /**
     * Called once click the zoom-out button
     * @param view: zoom-out button
     */
    public void zoomOut(View view){
        playButtonClickSound();
        statePlaying.keyDown(Constants.UserInput.ZoomOut, 0);
        Log.v(logTag, "zoom out");
    }

    /**
     * Called once click the toggle button to show show/hide the walls
     * Show the wall if the button is checked
     * Hide the wall otherwise.
     * @param view: toggle button
     */
    public void toggleShowWalls(View view){
        playButtonClickSound();
        statePlaying.keyDown(Constants.UserInput.ToggleLocalMap, 0);
        boolean toggleButtonIsChecked = toggleWalls.isChecked();
        if (toggleButtonIsChecked) {
            Log.v(logTag, "show walls");
        }
        else{
            Log.v(logTag, "hide walls");
        }
        //System.out.println(ToggleButtonState);
//        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
//                System.out.println("-----is checked-----");
//                System.out.println(isChecked);
//                if (isChecked) {
//                    Toast.makeText(getApplicationContext(), "show the currently visible walls", Toast.LENGTH_SHORT).show();
//                }
//                else{
//                    Toast.makeText(getApplicationContext(), "hide the currently visible walls", Toast.LENGTH_SHORT).show();
//                }
//            }
//        });
    }

    /**
     * Called once click the toggle button to show/hide the map
     * Show the map if the button is checked
     * Hide the map otherwise.
     * @param view: toggle button
     */
    public void toggleShowFullMaze(View view){
        playButtonClickSound();
        statePlaying.keyDown(Constants.UserInput.ToggleFullMap, 0);
        boolean toggleButtonIsChecked = toggleMap.isChecked();
        if (toggleButtonIsChecked) {
            Log.v(logTag, "show the whole maze");
        }
        else{
            Log.v(logTag, "hide the whole maze");
        }
    }

    /**
     * Called once click the toggle button to show/hide solution
     * Show the solution if the button is checked
     * Hide the solution otherwise.
     * @param view:toggle button
     */
    public void toggleShowSolution(View view){
        playButtonClickSound();
        statePlaying.keyDown(Constants.UserInput.ToggleSolution, 0);
        boolean toggleButtonIsChecked = toggleSolution.isChecked();
        if (toggleButtonIsChecked) {
            Log.v(logTag, "show the solution");
        }
        else{
            Log.v(logTag, "hide the solution");
        }
    }

    /**
     * Called once click the up button to move the robot move forward manually
     * @param view: up button
     */
    public void moveForward(View view){
        playButtonClickSound();
        statePlaying.keyDown(Constants.UserInput.Up, 0);
        Log.v(logTag, "manually move forward");
    }
    /**
     * Called once click the left button to move the robot turn left manually
     * @param view: left button
     */
    public void moveLeft(View view){
        playButtonClickSound();
        statePlaying.keyDown(Constants.UserInput.Left, 0);
        Log.v(logTag, "manually move left");
    }
    /**
     * Called once click the right button to move the robot turn right manually
     * @param view: right button
     */
    public void moveRight(View view){
        playButtonClickSound();
        statePlaying.keyDown(Constants.UserInput.Right, 0);
        Log.v(logTag, "manually move right");
    }


    /**
     * Called once click the back button
     * Return to the state tile state
     * @param view: back button
     */
    public void switchToTitle(View view){
        playButtonClickSound();
        backgroundMediaPlayer.stop();
        Intent intent = new Intent(getApplicationContext(), AMazeActivity.class);
        startActivity(intent);
        Log.v(logTag, "switch to title from state PlayManually");
    }



    /**
     * Create a bundle to store the information battery level, path length, and the shortest path length,
     * which can be used to pass to an intent so that information can be passed to winning/losing state
     */
    private void setWinningLosingInformationBundle(){
        bundle = new Bundle();
        bundle.putFloat(batteryLevelMessage, batteryLevel);
        bundle.putInt(pathLengthMessage, pathLength);
        bundle.putInt(shortestPathMessage, shortestPathLength);
        Log.v(logTag, "game information saved to bundle to be passed to winning or losing activity");
    }

    /**
     * A place holder button for project 6 only, helping to switch to winning state
     * @param
     *
     */
    public void switchFromPlayingToWinning(int pathLength, float batteryLevel){
        this.batteryLevel = 3000 - batteryLevel;
        this.pathLength = pathLength;
        this.setWinningLosingInformationBundle();
        backgroundMediaPlayer.stop();
        Intent intent = new Intent(getApplicationContext(), WinningActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        Log.v(logTag, "switch to state winning from state PlayManually");
    }

    /**
     * A place holder button for project 6 only, helping to switch to winning state
     * @param
     */
    public void switchFromPlayingToLosing(int pathLength, float batteryLevel){
        this.batteryLevel = 3000 - batteryLevel;
        this.pathLength = pathLength;
        this.setWinningLosingInformationBundle();
        backgroundMediaPlayer.stop();
        Intent intent = new Intent(getApplicationContext(),  LosingActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        Log.v(logTag, "switch to state losing from state PlayManually");
    }

    private void playButtonClickSound(){
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        mediaPlayer = MediaPlayer.create(PlayManuallyActivity.this, button_click);
        mediaPlayer.setVolume(1000, 1000);
        mediaPlayer.start();
        if (mediaPlayer.isPlaying()){
            Log.v(logTag, "button click media play has started");
        }
    }
}
