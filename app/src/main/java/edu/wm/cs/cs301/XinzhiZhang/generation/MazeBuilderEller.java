package edu.wm.cs.cs301.XinzhiZhang.generation;

import java.util.*;

/**
 * This class has the responsibility to implement the Eller algorithm to generate mazes
 * Starting from the first row, it randomly groups cells into sets
 * and randomly merger two cells in both horizontal and vertical direction 
 * Finally, it deletes all walls in the last row.
 * We use a 2D array to represent the cells and use an arraylist to store the information of sets.
 * We also make sure the walls on the border are not deleted and borders of rooms are deleted in only necessary circumstances.
 * @author Vivian Zhu and Lulu Zhang
 *
 */


public class MazeBuilderEller extends MazeBuilder implements Runnable{

	//the mazeArray keeps track of the cells, the content of the array keeps track of the arrayList that each
	//element belongs to
	protected int[][] mazeArray;
	
	//the connectedSets keeps track of all the cell positions that belongs to one set. 
	//instead of keeping track of just the current row, We choose to keep track all the rows from previous iterations
	//for simplicity of coding
	protected ArrayList<ArrayList<int[]>> connectedSets = new  ArrayList<ArrayList<int[]>>();
	
	
	//self initialize methods 
	public MazeBuilderEller() {
		super();
		System.out.println("MazeBuilderEller uses Eller's algorithm to generate maze.");
	}
	
	public MazeBuilderEller(boolean det, int seed) {
		super(det, seed);
		System.out.println("MazeBuilderEller uses Eller's algorithm to generate maze.");
	}
	
	
	/* For this algorithm: 
	 * 1. generate the first row, then randomly join adjacent cells (knock down walls)
	 * 2. put the joined cells in the same set. This indicates that all cells in both sets are now connected
	 * 3. then continue to create the next set of row, 
	 * 4. For this next set, we need to randomly create vertical connections downwards
	 * 	  and each remaining set must have at least one vertical connection
	 * 5. Then put any remaining cells into their own sets. 
	 * 6. Note: since we only have to consider the previous set, it is ok to just
	 * 		just keep the arraylist that only keep tracks of the informations;
	 * 
	 */
	@Override
	protected void generatePathways() {
		
		//initialize the mazeArray after the MazeBuilder has been set, it will be
		//in the same dimension as the floorPlan. 
		//Then simply populate all elements in this array with different integers. 
		this.populateMazeArray(this.width, this.height);
		
		/*This for loop will iterate through all rows of cells except the last one
		 * it will first connect the cells left and right
		 * and then connect to vertical cells. 
		*/
		for (int i = 0; i < this.width-1; i++) {
			this.connectAdjacentCells(i);
			this.connectVerticalCells(i);
		}
		
		//This method will connect all the cells in the last row togther, as described
		//in the Eller's Algorithm
		this.populateLastRow();
	}
	
	/** This method is used to connect the adjacent cells in one entire row. 
	 * Notice: the CardinalDirection.South should be used in here, instead of East because South will actually
	 *  do nothing to the x coordinate and increment the y coordinate, which is actually what we want. 
	 * 
	 * This function will do the following: 
	 * 1. determine the position of current cells and the adjacent (South) cells
	 * 2. Using the SingleRandom to determine if the wall should be deleted 
	 * 3. if this wall can be deleted, use the floorPlan field function to delete the wall
	 * 4. and then update mazeArray and CurrentSet accordingly
	 * 5. Or, if the Random number is telling to not delete the cell, then just check and add 
	 *    the cells to the currentSet, and then proceed to the next cell. 
	 * 
	 * @param i is the current row that we are located in right now
	 */
	protected void connectAdjacentCells(int i) {
		for (int j = 0; j < this.height-1 ; j++) {
			int [] arr1 = new int[] {i,j};
			int [] arr2 = new int[] {i,j+1};
			
			//create a random number between 0 and 1, if the number is 1, delete the wall
			int rand = random.nextIntWithinInterval(0, 1);
			Wallboard wallboard = new Wallboard(i,j,CardinalDirection.South);
			if ((this.mazeArray[i][j] != this.mazeArray[i][j+1])&&(rand == 1) && floorplan.canTearDown(wallboard)) {
				
				this.floorplan.deleteWallboard(wallboard);
				//update the MazeArray and ConnectedSets
				this.updateConnectedSetsHorizontal(arr1, arr2);
			}
			
			//if the random number tells us to not remove the wall, check if the positions are 
			//already in the currentArrays, and them if they are not already present. 
			else {
				if (this.checkIfCoordinateInConnectedSets(arr1) == -1) {
					this.createNewArrayListAndAddToConnectedSets(arr1);
				}
				if (this.checkIfCoordinateInConnectedSets(arr2) == -1) {
				this.createNewArrayListAndAddToConnectedSets(arr2);
				}
			}	
		}
	}

	/**
	 * This function merges two sets in the horizontal direction, i.e., in a row，
	 * by updating the cells contained in a set
	 * and updating the number contained in the cells to be the set number
	 * @param arr1
	 * @param arr2
	 */
	protected void updateConnectedSetsHorizontal(int[] arr1, int [] arr2) {
		//get the set numbers of two adjacent cells
		int index1 = this.checkIfCoordinateInConnectedSets(arr1);
		int index2 = this.checkIfCoordinateInConnectedSets(arr2);
		//get the set the cells belong to
		ArrayList<int []> list1 = this.getArrayListFromConnectedSets(arr1);
		ArrayList<int []> list2 = this.getArrayListFromConnectedSets(arr2);
		
		//When both the cells are not contained in any sets
		//put them in an ArrayList, i.e., make a set which contains the cells
		//update the number contained in both cells to the set index in which they belong to
		if (list1 == null && list2 == null) {
			ArrayList<int[]> tempArr = new ArrayList<int[]>();
			tempArr.add(arr1);
			tempArr.add(arr2);
			this.connectedSets.add(tempArr);
			
			int index = this.connectedSets.indexOf(tempArr);
			this.updateMazeArrayEntries(arr1, arr2, index);
		}
		//When cell1(represented by arr1) is in a set and cell2 is not contained in any set
		//add arr2 to arr1's set
		//for the cell in MazeArray that is represented by arr2
		//change the value contained in the cell to the set index
		//and vice versa
		else if (list1!= null&& list2 == null) {
			this.connectedSets.get(index1).add(arr2);
			this.mazeArray[arr2[0]][arr2[1]] = index1;
		}
		else if (list1 == null && list2!= null){
			this.connectedSets.get(index2).add(arr1);
			this.mazeArray[arr1[0]][arr1[1]] = index2;
		}
		//When the two cells are contained in different sets
		//add all cells in a set to another set
		//delete the "died" set
		//then make all cells now contained in the survived set to have the set number in MazeArray
		else if (list1 != null && list2 != null) {
			if (index1<index2) {
				list1.addAll(list2);
				this.connectedSets.remove(list2);
				for (int x = 0; x < list1.size(); x++) {
					this.mazeArray[list1.get(x)[0]][list1.get(x)[1]] = index1;
				}
			}
			
			else if (index1 > index2) {
				list2.addAll(list1);
				this.connectedSets.remove(list1);
				for (int x = 0; x < list2.size(); x++) {
					this.mazeArray[list2.get(x)[0]][list2.get(x)[1]] = index2;
				}
			}
			
		}
	}

	/**
	 * Create a new set contains the passed cell, add the set to connectedSet
	 * and update the number contained in the cell to be the set number in MazeArray 
	 * @param arr
	 */
	protected void createNewArrayListAndAddToConnectedSets(int [] arr) {
		if (this.checkIfCoordinateInConnectedSets(arr) == -1) {
			ArrayList<int[]> tempList= new ArrayList<int[]>();
			tempList.add(arr);
			this.connectedSets.add(tempList);
			this.mazeArray[arr[0]][arr[1]] = this.connectedSets.indexOf(tempList);
		}
		else {}
	}
	
	/**
	 * With the given cell and the index of the set which contains the cell return the set
	 * If the cell does not belong to any set, return null
	 * @param arr
	 * @return set the current cell belongs to or null
	 */
	protected ArrayList<int []> getArrayListFromConnectedSets(int[] arr) {
		int index = this.checkIfCoordinateInConnectedSets(arr);
		if (index > -1 && index< this.connectedSets.size()) {
			return this.connectedSets.get(index);
		}
		return null;
		
	}
	
	/**
	 * This function update the index of both position 1 and 2. 
	 * @param arr1
	 * @param arr2
	 * @param index
	 */
	protected void updateMazeArrayEntries(int[] arr1, int[]arr2, int index) {
		this.mazeArray[arr1[0]][arr1[1]] = index;
		this.mazeArray[arr2[0]][arr2[1]] = index;
	}
	

	/**
	 * Randomly choose cells in the current row to connect to cells in the next row
	 * make sure each set has at least on cell that is connected to the next row to avoid isolated sets
	 * @param i (represents the current row we are looking at)
	 */
	protected void connectVerticalCells(int i) {
		for (ArrayList<int[]> innerList: this.connectedSets) {
			ArrayList<int[]> ListToGetRandomArray = new ArrayList<int[]>();
			//Add all cells in the current row to a list which contains
			//potential cells that can extend to the next row
			for (int[] position:innerList) {
				if (position[0] == i) {
					ListToGetRandomArray.add(position);
				}
			}
			//Generate a random number in interval [1, length of number of cells]
			//to determine how many cells in a set will extend to the next row
			//Random rand = new Random();
			
			int rand1 = random.nextIntWithinInterval(0, ListToGetRandomArray.size()-1);
			int [] toConnect = ListToGetRandomArray.get(rand1);
			int [] belowToConnect = new int[] {toConnect[0]+1, toConnect[1]};
			
			Wallboard wallboard = new Wallboard(toConnect[0], toConnect[1], CardinalDirection.East);
			this.floorplan.deleteWallboard(wallboard);
			innerList.add(belowToConnect);
			this.mazeArray[belowToConnect[0]][belowToConnect[1]] = this.connectedSets.indexOf(innerList);

		}	
	}
	
	/**
	 * Change the original random/meaningless number contained in both of the adjacent cells
	 * to the index of the set which contains the cell
	 * Allow quick reference to the set that contains the cell
	 * @param arr1
	 * @param arr2
	 * @param index
	 */
	protected void updateConnectedSetVertical(int[] arr1, int[]arr2, int index) {
		this.connectedSets.get(index).add(arr2);
		this.mazeArray[arr2[0]][arr2[1]] = index;
	}
	
	/**
	 * Iterate through all sets in connectedSets to check 
	 * if the current cell in MazeArray is in a set
	 * and if it is in a set, return the set number
	 * if it does not belong to any set, return -1
	 * @param arr
	 * @return set number or -1
	 */
	protected int checkIfCoordinateInConnectedSets(int[] arr) {
		for (int i = 0; i < this.connectedSets.size(); i++) {
			for (int j = 0; j < this.connectedSets.get(i).size(); j++) {
				if (Arrays.equals(this.connectedSets.get(i).get(j), arr)) {
					return i;
				}
			}
		}
		return -1;
	}
	
	/**
	 * Delete walls between all adjacent (but disjoint) cells in the last row
	 */
	protected void populateLastRow() {
		//delete walls that are not borders in the last row
		for (int i = 0; i < this.height-1; i++) {
			Wallboard wallboard = new Wallboard(this.width-1,i,CardinalDirection.South);
			this.floorplan.deleteWallboard(wallboard);
		}

		int index = this.mazeArray[this.width-1][0];
		for (int i = 1; i< this.height; i++) {
			if (index > this.mazeArray[this.width-1][i]) {
				index = this.mazeArray[this.width-1][i];
			}
		}
		
		
		for (int i = 0; i < this.height; i++) {
			this.mazeArray[width-1][i] = index;
			int [] tempArray = new int[] {width -1, i};
			this.connectedSets.get(index).add(tempArray);
		}
		
			
	}
		
	/**
	 * This method populates the cells with different integers
	 * @param width
	 * @param height
	 */
	protected void populateMazeArray(int width, int height) {
		mazeArray = new int[width][height];
		int count = 0;
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				this.mazeArray[i][j] = count;
				count++;
			}
		}
	}

	
	
}
