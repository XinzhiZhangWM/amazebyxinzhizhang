package edu.wm.cs.cs301.XinzhiZhang.gui;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.graphics.Shader;
import android.media.Image;
import android.util.AttributeSet;
import android.view.View;

import edu.wm.cs.cs301.XinzhiZhang.R;

/**
 * Add functionality for double buffering to an AWT Panel class.
 * Used for drawing a maze.
 *
 * @author Peter Kemper
 *
 */
public class MazePanel extends View {
    /* Panel operates a double buffer see
     * http://www.codeproject.com/Articles/2136/Double-buffer-in-standard-Java-AWT
     * for details
     */
    // bufferImage can only be initialized if the container is displayable,
    // uses a delayed initialization and relies on client class to call initBufferImage()
    // before first use
    private Image bufferImage;
    //protected static Graphics2D graphics; // obtained from bufferImage,
    // graphics is stored to allow clients to draw on the same graphics object repeatedly
    // has benefits if color settings should be remembered for subsequent drawing operations
    private static Color color;
    private static int r;
    private static int g;
    private static int b;
    private Canvas canvas;
    private Paint paint;
    private Bitmap bitmap;

    public MazePanel(Context context) {
        super(context);
        init();
    }

    public MazePanel(Context context, AttributeSet attrs){
        super(context, attrs);
        init();
    }

    private void init(){
        bitmap = Bitmap.createBitmap(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT, Bitmap.Config.ARGB_8888);
        canvas = new Canvas(bitmap);
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        //canvas.drawColor(Color.WHITE);
    }

    @Override
    protected void onDraw(Canvas canvas){
        canvas.drawBitmap(this.bitmap, 0, 0, this.paint);
    }

    /**
     * Test functionality of canvas and bitmap
     */
    private void draw(){
        paint.setColor(Color.RED);
        fillOval(1000, 1000, 100, 100);
        paint.setColor(Color.GREEN);
        fillOval(800, 800, 100, 100);
        paint.setColor(Color.YELLOW);
        int[] xpoints = {50, 650, 650, 50};
        int[] ypoints = {50, 50, 500, 500};
        fillPolygon(xpoints, ypoints, 4);
        paint.setColor(Color.BLUE);
        int [] xpoints2 = {100, 260, 420};
        int [] ypoints2 = {1100, 800, 1100};
        fillPolygon(xpoints2, ypoints2, 3);
        drawLine(800, 50, 1200, 100);
        drawLine(800, 100, 800, 500);
    }

    /**
     * Constructor. Object is not focusable.
     */
    //public MazePanel() {
//        setFocusable(false);
//        bufferImage = null; // bufferImage initialized separately and later
//        graphics = null;    // same for graphics
//    }

    /**
     * Method to draw the buffer image on a graphics object that is
     * obtained from the superclass.
     * Warning: do not override getGraphics() or drawing might fail.
     */
    public void update() {
        invalidate();
    }

    /**
     * Draws the buffer image to the given graphics object.
     * This method is called when this panel should redraw itself.
     * The given graphics object is the one that actually shows
     * on the screen.
     */
//    @Override
//    public void paint(Graphics g) {
//        if (null == g) {
//            System.out.println("MazePanel.paint: no graphics object, skipping drawImage operation");
//        } else {
//            g.drawImage(bufferImage, 0, 0, null);
//        }
//    }

    /**
     * Obtains a graphics object that can be used for drawing.
     * This MazePanel object internally stores the graphics object
     * and will return the same graphics object over multiple method calls.
     * The graphics object acts like a notepad where all clients draw
     * on to store their contribution to the overall image that is to be
     * delivered later.
     * To make the drawing visible on screen, one needs to trigger
     * a call of the paint method, which happens
     * when calling the update method.
     *
     * @return graphics object to draw on, null if impossible to obtain image
     */
//    public Graphics getBufferGraphics() {
    // if necessary instantiate and store a graphics object for later use
//        if (null == graphics) {
//            if (null == bufferImage) {
//                bufferImage = createImage(Constants.VIEW_WIDTH, Constants.VIEW_HEIGHT);
//                System.out.println(this.isDisplayable());
//
//                this.revalidate();
//                if (null == bufferImage) {
//                    System.out.println("Error: creation of buffered image failed, presumedly container not displayable");
//                    return null; // still no buffer image, give up
//                }
//            }
//            graphics = (Graphics2D) bufferImage.getGraphics();
//            if (null == graphics) {
//                System.out.println("Error: creation of graphics for buffered image failed, presumedly container not displayable");
//            } else {
//                // System.out.println("MazePanel: Using Rendering Hint");
//                // For drawing in FirstPersonDrawer, setting rendering hint
//                // became necessary when lines of polygons
//                // that were not horizontal or vertical looked ragged
//                graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
//                        RenderingHints.VALUE_ANTIALIAS_ON);
//                graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
//                        RenderingHints.VALUE_INTERPOLATION_BILINEAR);
//            }
//        }
//        return graphics;
//    }

    /**
     * Call the graphics object to draw a line between two points
     * @param x1 the first point's x coordinate.
     * @param y1 the first point's y coordinate.
     * @param x2 the second point's x coordinate.
     * @param y2 the second point's y coordinate.
     */
    public void drawLine(int x1, int y1, int x2, int y2) {
        paint.setStrokeWidth(6);
        paint.setStyle(Paint.Style.STROKE);
        canvas.drawLine(x1, y1, x2, y2, paint);
    }

    /**
     * Call the graphics object to fills a closed polygon defined by arrays of x and y coordinates.
     *
     * @param xPoints an array of x coordinates.
     * @param yPoints an array of y coordinates.
     * @param nPoints the total number of points.
     */
    public void fillPolygon(int[] xPoints, int[] yPoints, int nPoints) {
        paint.setStyle(Paint.Style.FILL);
        Path polygonPath = new Path();
        polygonPath.moveTo(xPoints[0], yPoints[0]);
        for (int i = 0; i < nPoints; i++){
            polygonPath.lineTo(xPoints[i], yPoints[i]);
        }
        polygonPath.lineTo(xPoints[0], yPoints[0]);
        canvas.drawPath(polygonPath, paint);
    }

    public void fillPolygonWithGraph(int[] xPoints, int[] yPoints, int nPoints) {
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setStyle(Paint.Style.FILL);
        Bitmap wallBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.wall);
        BitmapShader bitmapShader = new BitmapShader(wallBitmap, Shader.TileMode.CLAMP, Shader.TileMode.REPEAT);
        paint.setShader(bitmapShader);
        Path polygonPath = new Path();
        polygonPath.moveTo(xPoints[0], yPoints[0]);
        for (int i = 0; i < nPoints; i++){
            polygonPath.lineTo(xPoints[i], yPoints[i]);
        }
        polygonPath.lineTo(xPoints[0], yPoints[0]);
        canvas.drawPath(polygonPath, paint) ;
    }


    /**
     * Called in Wall.java. Return the color of a specific wall.
     *
     * @return color object
     */
    public Color getColor() {
        return this.color;
    }

    /**
     * Create a color object with the specified red, green, and blue values in the range (0 - 255).
     * Set the created color to the graphics object.
     *
     * @param r the red component
     * @param g the green component
     * @param b the blue component
     */
    public void setColor(int r, int g, int b, int alpha) {
        this.r = r;
        this.g = g;
        this.b = b;
        int colorInt = color.rgb(r, g, b);
        paint.setColor(colorInt);
        paint.setAlpha(alpha);
    }

    /**
     * Return an integer array to notify red, green, and blue values of a color
     *
     * @return an integer array containing red, green, and blue values of a color.
     */
    public static int[] getRGB() {
        int[] rgb = {MazePanel.r, MazePanel.g, MazePanel.b};
        return rgb;
    }

    public static int getColorInt(int r, int g, int b){
        int colorInt = MazePanel.color.rgb(r, g, b);
        return colorInt;
    }

    /**
     * Create a color object with a combined RGB value.
     * Set the created color to the graphics object.
     * @param rgb
     */
    public void setColor(int rgb) {
        paint.setColor(rgb);
    }

    /**
     * Call a graphics object to fill an oval bounded by the specified rectangle with the current color.
     * @param x      the x coordinate of the upper left corner of the oval to be filled.
     * @param y      the y coordinate of the upper left corner of the oval to be filled.
     * @param width  the width of the oval to be filled.
     * @param height the height of the oval to be filled.
     */
    public void fillOval(int x, int y, int width, int height) {
        paint.setStyle(Paint.Style.FILL);
        RectF rectF = new RectF(x, y, x+width, y+height);
        canvas.drawOval(rectF, paint);
    }

    /**
     * Call a graphics object to fill the specified rectangle.
     * @param x      the x coordinate of the rectangle to be filled.
     * @param y      the y coordinate of the rectangle to be filled.
     * @param width  the width of the rectangle to be filled.
     * @param height the height of the rectangle to be filled.
     */
    public void fillRect(int x, int y, int width, int height) {
        paint.setStyle(Paint.Style.FILL);
        RectF rectF = new RectF(x, y, x+width, y+height);
        canvas.drawRect(rectF, paint);
    }

    /**
     * Return bitmap
     * @return bitmap
     */
    public Bitmap getBitmap() {
        return bitmap;
    }
}