package edu.wm.cs.cs301.XinzhiZhang.gui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;
import android.R.*;
import android.widget.AdapterView.OnItemSelectedListener;
import edu.wm.cs.cs301.XinzhiZhang.R;
import static edu.wm.cs.cs301.XinzhiZhang.R.raw.*;

/**
 * @author Lulu Zhang
 */

public class AMazeActivity extends AppCompatActivity implements OnItemSelectedListener{
    private SeekBar seekBar;
    private Spinner mazeGenerationSpinner;
    private Spinner driverSpinner;
    private int skillLevel;
    private String mazeGeneration;
    private String driver;
    private Intent intent;
    private Bundle bundle;
    private MediaPlayer mediaPlayer;
    private static final String logTag = "edu.wm.cs.cs301.XinzhiZhang.gui.AMazeActivity";
    private static final String skillLevelMessage = "AMazeActivity.SkillLevel";
    private static final String mazeGenMessage = "AMazeActivity.MazeGeneration";
    private static final String driverMessage = "AMazeActivity.Driver";
    private static final String revisitMaze = "AMazeActivity.revisitMaze";

    /**
     * Set the layout of the app to be the title layout
     * set seekbar, maze generation spinner, and driver spinner
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_amaze);
        setSeekBar();
        setMazeGenerationSpinner();
        setDriverSpinner();
    }

    /**
     * Obtain seekbar from the xml file and set seek bar change listeners to obtain the information
     * regarding when does touch start, on progress, and stop
     */
    private void setSeekBar(){
        seekBar = findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int Progress, boolean fromUser){
                Log.v(logTag, "seekbar progress: " + Progress);
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar){
                playButtonClickSound();
                Log.v(logTag, "seekbar touch started");
            }
            @Override
            public void onStopTrackingTouch(SeekBar seekBar){
                Log.v(logTag, "skill level is selected");
            }
        });
    }

    /**
     * Set maze generation spinner and add an array adapter to populate the spinner
     */
    private void setMazeGenerationSpinner() {
        mazeGenerationSpinner = findViewById(R.id.maze_generation_spinner);
        ArrayAdapter<CharSequence> mazeGenerationAdapter = ArrayAdapter.createFromResource(this, R.array.maze_generation, layout.simple_spinner_item);
        mazeGenerationAdapter.setDropDownViewResource(layout.simple_spinner_dropdown_item);
        mazeGenerationSpinner.setAdapter(mazeGenerationAdapter);
        mazeGenerationSpinner.setOnItemSelectedListener(this);
    }

    /**
     * Set driver spinner and add an array adapter to populate the spinner
     */
    private void setDriverSpinner(){
        driverSpinner = findViewById(R.id.driver_spinner);
        ArrayAdapter<CharSequence> driverAdapter = ArrayAdapter.createFromResource(this, R.array.driver, layout.simple_spinner_item);
        driverAdapter.setDropDownViewResource(layout.simple_spinner_dropdown_item);
        driverSpinner.setAdapter(driverAdapter);
        driverSpinner.setOnItemSelectedListener(this);
    }

    /**
     * Get the maze generation method and the driver from the user's input
     * Called when there is newly selected item or nothing is selected
     * @param parent: the array adapter view where the selection happens
     * @param view: the text view that was selected in the array adapter
     * @param position: the position of the view in the adapter
     * @param id: The row id of the item that is selected
     */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()){
            case R.id.maze_generation_spinner:
                mazeGeneration = parent.getItemAtPosition(position).toString();
                Log.v(logTag, parent.getItemAtPosition(position).toString() + " is selected");
                break;
            case R.id.driver_spinner:
                driver = parent.getItemAtPosition(position).toString();
                Log.v(logTag, parent.getItemAtPosition(position).toString() + " is selected");
                break;
        }
    }

    /**
     * Called when the selection disappears
     * Not really used
     * Implemented because AdapterView.OnItemSelectedListener interface has this method
     * @param parent:the array adapter view where the selection happens
     */
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        switch (parent.getId()) {
            case R.id.maze_generation_spinner:
                mazeGeneration = "DFS";
                break;
            case R.id.driver_spinner:
                driver = "Manual";
                break;
        }
    }

    /**
     * A helper function to pass the maze configuration information to the GeneratingActivity
     * called by revisit() and explore() when user press revisit or explore button
     */
    private void setMazeConfiguration() {
        skillLevel = seekBar.getProgress();
        intent = new Intent(this, GeneratingActivity.class);
        bundle = new Bundle();
        bundle.putInt(skillLevelMessage, skillLevel);
        bundle.putString(mazeGenMessage, mazeGeneration);
        bundle.putString(driverMessage, driver);
    }

    /**
     * Called by the revisit button to reload a maze.
     * Create a new intent to pass the user inputs from AMazeActivity to GeneratingActivity
     * so that a maze can be generated accordingly
     * @param view: the explore button
     */
    public void revisit(View view){
        Button revisitButton = findViewById(R.id.revisit_button);
//        MediaPlayer mediaPlayer = MediaPlayer.create(this, button_click);
//        revisitButton.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                try {
//                    if (mediaPlayer.isPlaying()) {
//                        mediaPlayer.stop();
//                        mediaPlayer.release();
//                    } mediaPlayer.start();
//                } catch(Exception e) { e.printStackTrace(); }
//            }
//            });
//        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
//            public void onCompletion(MediaPlayer mp) {
//                mediaPlayer.release();
//            }
//        });
        playButtonClickSound();
        setMazeConfiguration();
        bundle.putBoolean(revisitMaze, true);
        intent.putExtras(bundle);
        startActivity(intent);
        Log.v(logTag, "user revisits the maze");
    }

    /**
     * Called by the explore button
     * Create a new intent to pass the user inputs from AMazeActivity to GeneratingActivity
     * so that a maze can be generated accordingly
     * @param view: the explore button
     */
    public void explore(View view){
        setMazeConfiguration();
        playButtonClickSound();
        bundle.putBoolean(revisitMaze, false);
        intent.putExtras(bundle);
        startActivity(intent);
        Log.v(logTag, "user explores a new maze");
    }

    private void playButtonClickSound(){
        if (mediaPlayer != null) {
            mediaPlayer.reset();
            mediaPlayer.release();
            mediaPlayer = null;
        }
        mediaPlayer = MediaPlayer.create(AMazeActivity.this, button_click);
        mediaPlayer.setVolume(1000, 1000);
        mediaPlayer.start();
        System.out.println(mediaPlayer.isPlaying());
    }
}
